const express = require('express');
const postsRouter = require("./controllers/posts.controller");
const stuffRouter = require("./controllers/stuff.controller");

const app = express();
app.use(express.json());

const port = 3000;


app.use("/posts", postsRouter);
app.use("/stuff", stuffRouter);

app.listen(port, () => {
    console.log("Server was started.")
    console.log(`Listing at port ${port}`)
});