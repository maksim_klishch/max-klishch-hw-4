const express = require("express");
const fetch = require("node-fetch");
const router = express.Router();


router.get('/', (req, res) => {
    res.sendStatus(404)
})

router.get('/filtered', (req, res) => {
    fetch("https://jsonplaceholder.typicode.com/posts").then(response => {
        return response.json()
    }).then(posts => {
        res.json(posts.filter(post => post.title.includes(req.query.filter)))
    })
})



module.exports = router;