const express = require('express')
const router = express.Router()


router.get('/', (req, res) => {
    res.sendStatus(404)
})

router.get('/page', (req, res) => {
    res.send("This is stuff page!")
})

router.get('/data', (req, res) => {
    res.json({data: "This is stuff page!"})
})

router.post('/set', (req, res) => {
    if(Object.keys(req.body).length === 0){
        res.status(400).json({message: "No body passed!"})
    } else {
        res.json({message: "OK"})
    }
})


module.exports = router;